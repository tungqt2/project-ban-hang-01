
const pool = require('../../pgconnect')
var fs = require('fs')
var func = require('../../assets/func')
var encode_decode = require("../../assets/encode_decode")

module.exports = function(app) {

    app.post("/DSKhachHang/ThemKhachHang" , async (req,res)=>{
        try {
            const {token} = req.body
            fs.readFile('TenShop.txt', 'utf8', async (err, data)=>{
                const {ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email} = req.body 
                console.log({ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email})   
                if(err) throw res.json({
                    status:0,
                    message:'Hết phiên thao tác người dùng',
                    data:[]
                })
                const checkToken = await pool.query(`select token_te from token where token_te = N'${token.split("*")[0]}'`)
                
                if(token.split("*")[1] === data && checkToken.rowCount > 0){

                    const {ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email} = req.body 
                    console.log({ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email})   
                    console.log(data)
                    const checkUser = await pool.query(`
                        select * from khachhang where ten_kh = N'${ten_kh}'
                    `)

                    if(checkUser.rowCount > 0){
                        res.json({
                            status:0,
                            message:"Dữ liệu đã tồn tại",
                            data: []
                        })
                    }else{
                        const newQuery = await pool.query(`
                        insert into khachhang(ten_kh,dia_chi,so_dt,email,cmnd,ngay_sinh)
                        values(N'${ten_kh}',N'${dia_chi}',N'${so_dt}',N'${email}',N'${cmnd}','${ngay_sinh} 00:00:00')
                        `)
                        
                        if(newQuery.rowCount > 0){
                            const newData = await pool.query(`
                            select * from khachhang where ten_kh = N'${ten_kh}'
                            `)
                            res.json({
                                status:1,
                                message:'Thành công!',
                                data: newData.rows
                            })
                        }else{
                            res.json({
                                status:0,
                                message:'Lỗi phiên thao tác người dùng',
                                data:[]
                            })
                        }
                    }
                }else{
                    res.json({
                        status:0,
                        message:'Hết phiên thao tác người dùng',
                        data:[]
                    })
                }
            });
        } catch (error) {
            console.log(error)
            res.json({
                status:0,
                message:'Hết phiên thao tác người dùng',
                data:[]
            })
        }
    })

    app.put("/DSKhachHang/SuaKhachHang" , async (req,res)=>{
        try {
            const {token} = req.body
            
            fs.readFile('TenShop.txt', 'utf8', async (err, data)=>{
                if(err) throw res.json({
                    status:0,
                    message:'Hết phiên thao tác người dùng',
                    data:[]
                })
                const checkToken = await pool.query(`select token_te from token where token_te = N'${token.split("*")[0]}'`)
                console.log(data)
                if(token.split("*")[1] === data && checkToken.rowCount > 0){
                    const {ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email,id_kh} = req.body
                    console.log({ten_kh,dia_chi,so_dt,cmnd,ngay_sinh,email,id_kh})
                    const checkUser = await pool.query(`
                        select * from khachhang where id_kh = ${id_kh}
                    `)
                    
                    if(checkUser.rowCount === 0){
                        res.json({
                            status:0,
                            message:"Lỗi phiên thao tác người dùng",
                            data: []
                        })
                    }else{
                        const newQuery = await pool.query(`
                        update khachhang set ten_kh = N'${ten_kh}',
                        dia_chi = N'${dia_chi}', so_dt = N'${so_dt}',
                        email = N'${email}',cmnd = N'${cmnd}', ngay_sinh = '${ngay_sinh} 00:00:00'
                        where id_kh = ${id_kh}                        
                        `)
                        if(newQuery.rowCount > 0){
                            const newData = await pool.query(`
                            select * from khachhang where id_kh = ${id_kh}
                            `)
                            
                            res.json({
                                status:1,
                                message:'Thành công!',
                                data: newData.rows
                            })
                        }else{
                            res.json({
                                status:0,
                                message:'Lỗi phiên thao tác người dùng',
                                data:[]
                            })
                        }
                    }
                }else{
                    res.json({
                        status:0,
                        message:'Hết phiên thao tác người dùng',
                        data:[]
                    })
                }
            });
        } catch (error) {
            res.json({
                status:0,
                message:'Hết phiên thao tác người dùng',
                data:[]
            })
        }
    })

    app.delete("/DSKhachHang/XoaKhachHang" , async (req,res)=>{
        try {
            const {token} = req.body
            
            fs.readFile('TenShop.txt', 'utf8', async (err, data)=>{
                if(err) throw res.json({
                    status:0,
                    message:'Hết phiên thao tác người dùng',
                    data:[]
                })
                const checkToken = await pool.query(`select token_te from token where token_te = N'${token.split("*")[0]}'`)
                console.log(data)
                if(token.split("*")[1] === data && checkToken.rowCount > 0){

                    const {id_kh,ten_kh,mat_khau,ngay,ten_khach,loai_tk,vi_tien,email} = req.body    
                    const checkUser = await pool.query(`
                        select * from khachhang where id_kh = ${id_kh}
                    `)

                    if(checkUser.rowCount === 0){
                        res.json({
                            status:0,
                            message:"Lỗi phiên thao tác người dùng",
                            data: []
                        })
                    }else{
                        const newQuery = await pool.query(`
                            DELETE from khachhang where id_kh = ${id_kh}
                        `)
                        
                        if(newQuery.rowCount > 0){
                            res.json({
                                status:1,
                                message:'Thành công!',
                                data: id_kh
                            })
                        }else{
                            res.json({
                                status:0,
                                message:'Lỗi phiên thao tác người dùng',
                                data:[]
                            })
                        }
                    }
                }else{
                    res.json({
                        status:0,
                        message:'Hết phiên thao tác người dùng',
                        data:[]
                    })
                }
            });
        } catch (error) {
            res.json({
                status:0,
                message:'Hết phiên thao tác người dùng',
                data:[]
            })
        }
    })

    app.post('/DSKhachHang/:page' , async (req,res)=>{
        try {
            const {token} = req.body
            
            fs.readFile('TenShop.txt', 'utf8', async (err, data)=>{
                if(err) throw res.json({
                    status:0,
                    message:'Hết phiên thao tác người dùng',
                    data:[]
                })
                const checkToken = await pool.query(`select token_te from token where token_te = N'${token.split("*")[0]}'`)
                console.log(data)
                if(token.split("*")[1] === data && checkToken.rowCount > 0){

                    const {page} = req.params
                    const newQuery = await pool.query(`select * from khachhang LIMIT 10 OFFSET ${page === 1 ? 0 : parseInt(page - 1)*10}`)
                    res.json({
                        status:1,
                        message:'Thành công!',
                        data: newQuery.rows
                    })

                }else{
                    res.json({
                        status:0,
                        message:'Hết phiên thao tác người dùng',
                        data:[]
                    })
                }
            });
        } catch (error) {
            res.json({
                status:0,
                message:'Hết phiên thao tác người dùng',
                data:[]
            })
        }
    })

    

    app.get(`/TotalPageKhachHang` , async(req,res)=>{
        try {
            const newQuery = await pool.query(`
                select count(*) from khachhang
            `)

            res.json({
                status: newQuery.rowCount > 0 ? 1 : 0,
                data : Math.ceil(parseInt(newQuery.rows[0].count) /10),
                message :  newQuery.rowCount > 0 ? "Thành công!" : "Thất bại!"
            })
        } catch (error) {
            
        }
    })

    
}



