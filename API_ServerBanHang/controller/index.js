//#region API
const KhachHang = require("../api/KhachHang")
const NganhHang = require("../api/NganhHang")
const TaiKhoan = require("../api/TaiKhoan")
const Token = require("../api/Token");
const ThongBao = require("../socket/ThongBao");
//#region ENDAPI

//#region SOCKET IO

//#endregion
function Controller(app,io){
    Token(app);TaiKhoan(app);NganhHang(app);KhachHang(app)
    ThongBao(io)
}

module.exports = {
    Controller
}