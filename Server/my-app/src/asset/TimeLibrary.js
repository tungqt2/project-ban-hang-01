// const date = new Date("2021-07-02T06:52:09.000Z")
// const timeNow = `${(date).getDate()}-${(date).getMonth()+1}-${(date).getFullYear()} ${(date).getHours()}:${(date).getMinutes()}:${(date).getSeconds()}`
// const timeNowDB = `${(date).getFullYear()}-${(date).getMonth()+1}-${(date).getDate()} ${(date).getHours()}:${(date).getMinutes()}:${(date).getSeconds()}`
function convertTime(str){
    const date = new Date(str)
    // console.log()
    return `${(date).getDate()}-${(date).getMonth()+1}-${(date).getFullYear()} ${(date).getHours()}:${(date).getMinutes()}:${(date).getSeconds()}`
}
function convertTimeDateTime(time){
    return `${time.split(" ")[0].split("-")[2]}-${time.split(" ")[0].split("-")[1].length === 1 ? "0" : ""}${time.split(" ")[0].split("-")[1]}-${time.split(" ")[0].split("-")[0].length === 1 ? "0" : ""}${time.split(" ")[0].split("-")[0]}`
}
export default {convertTime,convertTimeDateTime}