var SHOP = "*Shop Ngọc Hà_ThanhTung0936563013"
var host = "http://127.0.0.1:3007"

var token = host +  "/Token/0366262072"
//#region  I. Tài Khoản
var DangNhap = host +  "/DangNhap"
var DSTaiKhoanThem = host + "/DSTaiKhoan/ThemTaiKhoan"
var DSTaiKhoanSua = host + "/DSTaiKhoan/SuaTaiKhoan"
var DSTaiKhoanXoa = host + "/DSTaiKhoan/XoaTaiKhoan"
var DSTaiKhoan = host + "/DSTaiKhoan"
var QuenMatKhau = host + "/QuenMatKhau"
var TotalPageTaiKhoan = host + "/TotalPageTaiKhoan"
//#endregion


//#region II. Khách hàng
var DSKhachHangThem = host + "/DSKhachHang/ThemKhachHang"
var DSKhachHangSua = host + "/DSKhachHang/SuaKhachHang"
var DSKhachHangXoa = host + "/DSKhachHang/XoaKhachHang"
var DSKhachHang = host + "/DSKhachHang"
var TotalPageKhachHang = host + "/TotalPageKhachHang"
//#endregion

//#region III. Ngành Hàng
var DSNganhHangThem = host + "/DSNganhHang/ThemNganhHang"
var DSNganhHangSua = host + "/DSNganhHang/SuaNganhHang"
var DSNganhHangXoa = host + "/DSNganhHang/XoaNganhHang"
var DSNganhHang = host + "/DSNganhHang"
var TotalPageNganhHang = host + "/TotalPageNganhHang"
//#endregion


export default {
    //// Tài khoản
    token,SHOP,DangNhap,DSTaiKhoanThem,
    DSTaiKhoan,QuenMatKhau,DSTaiKhoanSua,DSTaiKhoanXoa,TotalPageTaiKhoan,
    //// Khách hàng
    DSKhachHangThem,DSKhachHangSua,DSKhachHangXoa,DSKhachHang,TotalPageKhachHang,
    //// Ngành hàng
    DSNganhHangThem,DSNganhHangSua,DSNganhHangXoa,DSNganhHang,TotalPageNganhHang

}


//#region ==============================Mục lục==========================================
/*
    API kết nối CSDL
    I. Tài khoản (Dòng 5)
    II. Khách hàng
*/