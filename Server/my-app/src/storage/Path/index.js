import React from 'react'
import LoaiHang from '../../views/Main/DanhMuc/LoaiHang'
import MatHang from '../../views/Main/DanhMuc/MatHang'
import NganhHang from '../../views/Main/DanhMuc/NganhHang'
import KhachHang from '../../views/Main/QuanLy/KhachHang'
import TaiKhoan from '../../views/Main/QuanLy/TaiKhoan'

export default [
    //#region Quản lý
    {
        path:"/KhachHang",
        components:<KhachHang></KhachHang>
    },
    {
        path:"/TaiKhoan",
        components:<TaiKhoan></TaiKhoan>
    },
    //#endregion

    //#region Danh mục
    {
        path:"/NganhHang",
        components:<NganhHang></NganhHang>
    },
    {
        path:"/LoaiHang",
        components:<LoaiHang></LoaiHang>
    },
    {
        path:"/MatHang",
        components:<MatHang></MatHang>
    },
    //#endregion

    
    {
        path:"/",
        components:<div style={{height:'100vh'}}></div>
    }
]